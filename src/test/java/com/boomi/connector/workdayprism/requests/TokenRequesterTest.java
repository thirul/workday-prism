//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.workdayprism.requests;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.boomi.connector.workdayprism.model.Credentials;
import com.boomi.connector.workdayprism.responses.TokenResponse;
import com.boomi.connector.workdayprism.utils.TestConstants;
import com.boomi.util.StringUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author saurav.b.sengupta
 *
 */
public class TokenRequesterTest {

	@BeforeClass
	public static void init() {
		RequestContextHelper.setSSLContextForTest();
	}
	
	@Test
	public void testTokenRequest() throws IOException {
		Credentials credentials=new Credentials(TestConstants.API_ENDPOINT,TestConstants.CLIENT_ID, TestConstants.CLIENT_SECRET, TestConstants.REFRESH_TOKEN);
		TokenResponse tokenResponse = new TokenRequester(credentials).get();
		assertEquals(TestConstants.REFRESH_TOKEN, tokenResponse.getRefreshToken());
		assertEquals("Bearer", tokenResponse.getTokenType());
		System.out.println("ACCESS TOKEN: "+tokenResponse.getAccessToken());
		assertTrue(StringUtil.isBlank(tokenResponse.getAccessToken()));
	}
}
