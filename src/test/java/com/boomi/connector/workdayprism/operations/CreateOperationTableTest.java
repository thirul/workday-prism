//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.workdayprism.operations;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.workdayprism.PrismConnection;
import com.boomi.connector.workdayprism.model.PrismResponse;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;

public class CreateOperationTableTest {

	private Logger logger = mock(Logger.class);
	private OperationResponse operationResponse = mock(OperationResponse.class);
	private ObjectData objectData = mock(ObjectData.class, RETURNS_DEEP_STUBS);
	private UpdateRequest updateRequest = mock(UpdateRequest.class);
	private OperationContext opContext = mock(OperationContext.class);
	private PrismConnection connection = mock(PrismConnection.class, RETURNS_DEEP_STUBS);
	private PrismResponse prismResponse = mock(PrismResponse.class);

	@Before
	public void init() throws IOException {
		String inputJsonString = readJsonFromFile();
		InputStream inputStream = new ByteArrayInputStream(inputJsonString.getBytes(StandardCharsets.UTF_8));
		when(connection.getOperationContext()).thenReturn(opContext);
		when(opContext.getObjectTypeId()).thenReturn("dataset");
		when(operationResponse.getLogger()).thenReturn(logger);
		when(objectData.getData()).thenReturn(inputStream);
	}

	@Test
	public void testCreateOperationConnectionCall() {
		assertNotNull(new CreateOperation(connection).getConnection());
	}

	public String readJsonFromFile() {
		String text = null;
		try {
			text = new String(Files.readAllBytes(Paths.get("src/test/resources/" + "create_table.json")));

		} catch (IOException e) {
			logger.info("Error occured in Test class.");
		}
		return text;
	}

	@Test
	public void testAdditionOfApplicationErrorWhenInvalidInput() throws IOException {
		when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());
		when(connection.createTable(objectData)).thenReturn(prismResponse);
		new CreateOperation(connection).executeUpdate(updateRequest, operationResponse);
		verify(prismResponse).addResult(objectData, operationResponse);
		verify(connection).createTable(objectData);
	}

	@Test
	public void testAdditionOfApplicationErrorWhenAConnectorExceptionIsThrown() throws IOException {
		when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());

		Exception exception = new ConnectorException("");
		doThrow(exception).when(connection).createTable(objectData);

		new CreateOperation(connection).executeUpdate(updateRequest, operationResponse);

		verify(operationResponse).addResult(eq(objectData), eq(OperationStatus.APPLICATION_ERROR), eq(""),
				eq("Unknown failure"), (Payload) isNull(Payload.class));
	}
	
	@Test
    public void testFailWhenNullPointerExceptionIsThrown() throws IOException {
        when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());

        Exception exception = new NullPointerException();
        doThrow(exception).when(connection).createTable(objectData);

        new CreateOperation(connection).executeUpdate(updateRequest, operationResponse);

        verify(operationResponse).addErrorResult(eq(objectData), eq(OperationStatus.FAILURE), eq(""),
                eq("java.lang.NullPointerException"), eq(exception));
    }
	
	

}
