//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.workdayprism.operations;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when; 
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;

import java.io.IOException;
import java.util.Collections;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.workdayprism.PrismConnection;
import com.boomi.connector.workdayprism.model.PrismResponse;
import com.boomi.connector.workdayprism.model.UploadMetadata;
import com.boomi.connector.workdayprism.model.UploadResponse;
import com.boomi.connector.workdayprism.operations.upload.UploadHelper;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author saurav.b.sengupta
 *
 */
public class ImportOperationTest {
	 private Logger logger = mock(Logger.class);
		private OperationResponse operationResponse = mock(OperationResponse.class);
		private ObjectData objectData = mock(ObjectData.class, RETURNS_DEEP_STUBS);
		private UpdateRequest updateRequest = mock(UpdateRequest.class);
		private OperationContext opContext = mock(OperationContext.class);
		private PrismConnection connection = mock(PrismConnection.class, RETURNS_DEEP_STUBS);
		private PrismResponse prismResponse = mock(PrismResponse.class);
		private UploadResponse uploadResponse = mock(UploadResponse.class);
		private PropertyMap opProps=mock(PropertyMap.class);
		private UploadHelper uploadHelper;
		private CompleteBucketOperation completeBucketOperation;
		private UploadMetadata uploadMetadata;
		
		@Before
		public void init() throws IOException {
			when(operationResponse.getLogger()).thenReturn(logger);
			 when(opContext.getObjectTypeId()).thenReturn("import");
			 uploadHelper=new UploadHelper(connection);
			 completeBucketOperation=new CompleteBucketOperation(connection);
			 uploadMetadata=new UploadMetadata(objectData, opProps, anyString());
		}
		
		@Test
		public void testImportOperationCall() {
			assertNotNull(new ImportOperation(connection).getConnection());
		}
		
		@Test
		public void testExecuteUpdate() throws IOException { 
			assertNotNull(updateRequest);
			when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());
			when(connection.createBucket(any(JsonNode.class))).thenReturn(prismResponse);
			//when(new UploadHelper(connection)).thenReturn(uploadHelper);
			when(uploadHelper.upload(objectData, null)).thenReturn(uploadResponse);
			doNothing().when(completeBucketOperation).processInput(objectData, operationResponse, anyString());
			new ImportOperation(connection).executeUpdate(updateRequest, operationResponse);
			
		}
}
