//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.workdayprism.operations;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.workdayprism.PrismConnection;
import com.boomi.connector.workdayprism.model.PrismResponse;
import com.boomi.util.io.FastByteArrayInputStream;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.junit.Test;

import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CompleteBucketOperationTest {
    private static final String PROPERTY_WAIT_FOR_COMPLETION = "wait_for_completion";
    private static final String PROPERTY_TIMEOUT = "timeout";
    private static final String BUCKET_ID = "1234";
    private static final String INPUT_JSON = "{\"bucket_id\":\"" + BUCKET_ID + "\"}";


    private OperationResponse operationResponse = mock(OperationResponse.class);
    private ObjectData objectData = mock(ObjectData.class, RETURNS_DEEP_STUBS);
    private UpdateRequest updateRequest = mock(UpdateRequest.class);
    private PrismConnection connection = mock(PrismConnection.class, RETURNS_DEEP_STUBS);
    private PrismResponse prismResponse = mock(PrismResponse.class);

    private JsonNode buildStatusResponse(String condition) {
        ObjectNode json = JSONUtil.newObjectNode();
        json.putObject("state").put("descriptor", condition);
        return json;
    }

    @Test
    public void shouldCallConnection() {
        assertNotNull(new GetOperation(connection).getConnection());
    }

    @Test
    public void shouldAddApplicationErrorWhenInvalidInput() throws IOException {
        when(connection.getBooleanProperty(PROPERTY_WAIT_FOR_COMPLETION)).thenReturn(false);
        when(connection.getLongProperty(PROPERTY_TIMEOUT, 0L)).thenReturn(0L);
        when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());

        new CompleteBucketOperation(connection).executeUpdate(updateRequest, operationResponse);

        verify(connection).getBooleanProperty(PROPERTY_WAIT_FOR_COMPLETION);
        verify(connection).getLongProperty(PROPERTY_TIMEOUT, 0L);
        verify(operationResponse).addResult(eq(objectData), eq(OperationStatus.APPLICATION_ERROR), eq(""),
                anyString(), isNull(Payload.class));
    }

    @Test
    public void shouldAddResultWithoutWaitingForCompletion() throws IOException {
        assertNotNull(updateRequest);
    	when(connection.getBooleanProperty(PROPERTY_WAIT_FOR_COMPLETION)).thenReturn(false);
        when(connection.getLongProperty(PROPERTY_TIMEOUT, 0L)).thenReturn(0L);
        when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());
        when(objectData.getData()).thenReturn(new FastByteArrayInputStream(INPUT_JSON.getBytes()));
        when(connection.completeBucket(BUCKET_ID)).thenReturn(prismResponse);
        when(prismResponse.isSuccess()).thenReturn(true); 

        new CompleteBucketOperation(connection).executeUpdate(updateRequest, operationResponse);
    }

    @Test
    public void shouldNotWaitForCompletionWhenUnsuccessfulResponse() throws IOException {
    	assertNotNull(updateRequest);
        when(connection.getBooleanProperty(PROPERTY_WAIT_FOR_COMPLETION)).thenReturn(true);
        when(connection.getLongProperty(PROPERTY_TIMEOUT, 0L)).thenReturn(0L);
        when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());
        when(objectData.getData()).thenReturn(new FastByteArrayInputStream(INPUT_JSON.getBytes()));
        when(connection.completeBucket(BUCKET_ID)).thenReturn(prismResponse);
        when(prismResponse.isSuccess()).thenReturn(false);

        new CompleteBucketOperation(connection).executeUpdate(updateRequest, operationResponse);
    }

    @Test
    public void shouldWaitForCompletion() throws IOException {
    	assertNotNull(updateRequest);
        when(connection.getBooleanProperty(PROPERTY_WAIT_FOR_COMPLETION)).thenReturn(true);
        when(connection.getLongProperty(PROPERTY_TIMEOUT, 0L)).thenReturn(0L);
        when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());
        when(objectData.getData()).thenReturn(new FastByteArrayInputStream(INPUT_JSON.getBytes()));
        when(connection.completeBucket(BUCKET_ID)).thenReturn(prismResponse);
        when(connection.getBucket(BUCKET_ID)).thenReturn(prismResponse);
        when(prismResponse.isSuccess()).thenReturn(true);
        when(prismResponse.getJsonEntity()).thenReturn(buildStatusResponse("SUCCESS"));

        new CompleteBucketOperation(connection).executeUpdate(updateRequest, operationResponse);
    }

    @Test
    public void shouldWaitForCompletionAndGetFailedCondition() throws IOException {
    	assertNotNull(updateRequest);
    	when(connection.getBooleanProperty(PROPERTY_WAIT_FOR_COMPLETION)).thenReturn(true);
        when(connection.getLongProperty(PROPERTY_TIMEOUT, 0L)).thenReturn(0L);
        when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());
        when(objectData.getData()).thenReturn(new FastByteArrayInputStream(INPUT_JSON.getBytes()));
        when(connection.completeBucket(BUCKET_ID)).thenReturn(prismResponse);
        when(connection.getBucket(BUCKET_ID)).thenReturn(prismResponse);
        when(prismResponse.isSuccess()).thenReturn(true);
        when(prismResponse.getJsonEntity()).thenReturn(buildStatusResponse("FAILED"));

        new CompleteBucketOperation(connection).executeUpdate(updateRequest, operationResponse);

    }

    @Test
    public void shouldFailureWhenExceptionIsThrown() throws IOException {
    	assertNotNull(updateRequest);
    	when(updateRequest.iterator()).thenReturn(Collections.singletonList(objectData).iterator());

        Exception exception = new NullPointerException();
        doThrow(exception).when(objectData).getData();

        new CompleteBucketOperation(connection).executeUpdate(updateRequest, operationResponse);

    }
}
