//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.workdayprism.operations;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.GetRequest;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.workdayprism.PrismConnection;
import com.boomi.connector.workdayprism.model.PrismResponse;
import com.boomi.connector.workdayprism.operations.GetOperation;
import org.junit.Test;
import java.io.IOException;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * @author saurav.b.sengupta
 *
 */
public class GetOperationTest {

    private static final String NOT_FOUND = "404";
    private static final String BUCKET_ID = "bucketId";

    private OperationResponse operationResponse = mock(OperationResponse.class);
    private ObjectIdData objectIdData = mock(ObjectIdData.class, RETURNS_DEEP_STUBS);
    private GetRequest getRequest = mock(GetRequest.class);
    private PrismConnection connection = mock(PrismConnection.class, RETURNS_DEEP_STUBS);
    private PrismResponse prismResponse = mock(PrismResponse.class);

    @Test
    public void testGetConnectionCall() {
        assertNotNull(new GetOperation(connection).getConnection());  
    }

    @Test
    public void testAdditionOfApplicationErorWhenGetObjectReturnsNotFound() throws IOException {
        assertNotNull(getRequest);
    	when(getRequest.getObjectId()).thenReturn(objectIdData);
        when(objectIdData.getObjectId()).thenReturn(BUCKET_ID);
        when(connection.getBucket(eq(BUCKET_ID))).thenReturn(prismResponse);
        when(prismResponse.isNotFound()).thenReturn(true);

        new GetOperation(connection).executeGet(getRequest, operationResponse);
    } 

    @Test
    public void testAdditionOfApplicationErrorWhenEmptyBucketIdEntered() throws IOException {
        when(getRequest.getObjectId()).thenReturn(objectIdData);
        when(objectIdData.getObjectId()).thenReturn(null);

        new GetOperation(connection).executeGet(getRequest, operationResponse);

        verify(operationResponse).addResult(eq(objectIdData), eq(OperationStatus.APPLICATION_ERROR), eq(""),
                eq("the ID parameter is empty or only contains blank spaces"), isNull(Payload.class));
    }

    @Test
    public void testAdditionOfApplicationErrorWhenAConnectorExceptionIsThrown() throws IOException {
        when(getRequest.getObjectId()).thenReturn(objectIdData);
        when(objectIdData.getObjectId()).thenReturn(BUCKET_ID);
        doThrow(new ConnectorException("")).when(connection).getBucket(eq(BUCKET_ID));

        new GetOperation(connection).executeGet(getRequest, operationResponse);

        verify(operationResponse).addResult(eq(objectIdData), eq(OperationStatus.APPLICATION_ERROR), eq(""),
                eq("Unknown failure"), isNull(Payload.class));
    }

    @Test
    public void testFailureWhenExceptionIsThrown() throws IOException {
        when(getRequest.getObjectId()).thenReturn(objectIdData);
        when(objectIdData.getObjectId()).thenReturn(BUCKET_ID);
        Exception exception = new NullPointerException();
        doThrow(exception).when(connection).getBucket(eq(BUCKET_ID));

        new GetOperation(connection).executeGet(getRequest, operationResponse);

        verify(operationResponse).addErrorResult(eq(objectIdData), eq(OperationStatus.FAILURE), eq(""),
                eq("java.lang.NullPointerException"), eq(exception));
    }
}
