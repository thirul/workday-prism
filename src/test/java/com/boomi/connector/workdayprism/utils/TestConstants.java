//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.workdayprism.utils;

public class TestConstants {

    public static final String PROP_API_ENDPOINT = "endpoint";
    public static final String PROP_CLIENT_ID = "clientId";
    public static final String PROP_CLIENT_SECRET = "clientSecret";
    public static final String PROP_REFRESH_TOKEN = "refreshToken";

    public static final String API_ENDPOINT = "https://wd2-impl-services1.workday.com/ccx/api/v2/dellboomi_pt1 ";
    public static final String CLIENT_ID = "MWNkYjUwMjEtNGRkYy00NmE3LThkZGMtODU2NGQyYjAwMTRj";
    public static final String CLIENT_SECRET =
            "33puflqew3589nkug186cqlzemsunpjhyh78gaj2pfdf28qb6keo4eyjrxjuxf0t4lb2uzzknu6q1an3080m3el5jyj5ceyesfy";
    public static final String REFRESH_TOKEN =
            "11pmtk1uihmxjqrt949hf8m0zzt71izkt337117wi12lwzlov9ah076l67pf0y5jemy3hmr3pjo9vw5xdltqn2p7uipzq58sqe1g";
    public static final String ACCESS_TOKEN = "access";

    private TestConstants() {
    }
}
