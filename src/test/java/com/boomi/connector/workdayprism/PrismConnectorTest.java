// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.workdayprism;

import org.junit.Test;
import com.boomi.connector.workdayprism.operations.GetOperation;
import com.boomi.connector.workdayprism.utils.PrismITContext;
import static org.junit.Assert.assertTrue;

/**
 * @author saurav.b.sengupta
 *
 */
public class PrismConnectorTest {
	
	PrismITContext context=new PrismITContext();
	PrismConnector connector=new PrismConnector();
	
	
	/**
	 * Test method to validate browser instance creation functionality
	 */

	@Test
	public void shouldCreateBrowser() {
		assertTrue(connector.createBrowser(context) instanceof PrismBrowser);
	}
	
	/**
	 * Test method to validate browser instance creation functionality
	 */
	@Test
	public void shouldCreateGetOperation() {
		assertTrue(connector.createGetOperation(context) instanceof GetOperation);
	}
	
    /**
     * Test method to validate execute operation and expecting an expection
     */
    @Test (expected = UnsupportedOperationException.class)
    public void shouldThrowExceptionInExecutingOperation() {
    	connector.createExecuteOperation(context);
    }	
	

}
